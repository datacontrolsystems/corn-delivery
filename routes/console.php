<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('clear:logs', function () {
    \Illuminate\Support\Facades\File::deleteFilesByPattern(storage_path('logs/*.log'));
})->describe('Delete all log files.');

Artisan::command('clear:caches', function () {
    $this->call('optimize:clear');
    try {
        $this->callSilently('permission:cache-reset');
    } catch (Exception $e) {
    }
})->describe('Clear all application caches');

Artisan::command('ide-helper:all', function () {
    rescue(function () {
        $this->call('ide-helper:eloquent');
        $this->call('ide-helper:model', ['--reset' => true, '--write' => true]);
        $this->call('ide-helper:generate');
        $this->call('ide-helper:meta');
    });
})->describe('Call all ide-helper functions.');


Artisan::command('db:seed:permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions');

Artisan::command('db:seed:roles-permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultRolesSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultRolesWithPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions, roles and their associations');

Artisan::command('db:seed:config', function () {
    $this->call('config:clear');
    $this->call('db:seed', ['--class' => 'DefaultConfigSeeder', '--force' => true]);
    $this->call('optimize');
})->describe('ReSeed configuration');

Artisan::command('exception:list', function () {
    $this->table(['Exception', 'Status', 'Message'], app('exceptions')->map(function ($class) {
        $e = (new \ReflectionClass($class))->newInstanceWithoutConstructor();
        return [
            class_basename($class),
            $e->status,
            $e->getLocalizedMessage(),
        ];
    })->toArray());
})->describe('List all custom exception with their translation and status code.');

