<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['throttle:api'])->get('/time', [\App\Http\Controllers\API\InfoController::class, 'time'])->name('time');

Route::middleware(['auth:clients', 'throttle:clients', 'verified-phone'])->group(function () {

    Route::prefix('clients')->name('clients.')->group(function () {
        Route::get('/current', [\App\Http\Controllers\API\ClientController::class, 'current'])->name('current');
        Route::post('/register', [\App\Http\Controllers\API\ClientController::class, 'register'])->name('register')->withoutMiddleware(['auth:clients', 'verified-phone']);
        Route::post('/verify/{client_phone}', [\App\Http\Controllers\API\ClientController::class, 'verifyPhone'])->name('verify')->withoutMiddleware(['auth:clients', 'verified-phone']);
    });

    Route::apiResource('products', \App\Http\Controllers\API\ProductController::class)->only('index');

    Route::get('orders/last', [App\Http\Controllers\API\OrderController::class, 'showLast'])->name('orders.last');
    Route::apiResource('orders', App\Http\Controllers\API\OrderController::class)->only('index', 'show', 'store');

});
