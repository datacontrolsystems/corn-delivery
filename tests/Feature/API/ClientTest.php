<?php

namespace Tests\Feature\API;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ClientTest extends TestCase
{
    public function test_register()
    {
        $response = $this->post('/api/clients/register', [
            'name'  => 'John',
            'phone' => '6912345678'
        ]);

        $response->assertStatus(200);
    }
}
