<?php

namespace App\Exceptions;

use Exception;

class InvalidDataException extends BaseException
{
    public int $status = 422;

}
