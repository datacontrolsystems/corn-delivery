<?php

namespace App\Exceptions;
use App\Models\Client;

/**
 * @property Client $client
 */
class PhoneVerificationException extends BaseException
{
    public int $status = 422;
}
