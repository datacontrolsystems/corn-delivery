<?php

namespace App\Exceptions;

use Vonage\Verify\Verification;

class VerificationServiceException extends BaseException
{
    public static function fromResponse(Verification $response)
    {
        return new static(['request_id' => $response->getRequestId(), 'response_status' => $response->getStatus(), 'response_error_text' => array_get($response->getResponseData(), 'error_text')]);
    }
}
