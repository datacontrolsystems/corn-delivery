<?php

namespace App\Exceptions;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

/**
 * @property string $exception
 * @property string|null $exception_id
 * @property string $localized_message
 */
abstract class BaseException extends \Exception implements Arrayable, \JsonSerializable, Jsonable, \Serializable, HttpExceptionInterface
{
    public int $status = 503;
    public array $context = [];
    public array $headers = [];

    public function __isset($name)
    {
        return property_exists($this, $name) || isset($this->context[$name]);
    }

    public function __unset($name)
    {
        if (property_exists($this, $name))
            unset($this->{$name});
        if (isset($this->context[$name]))
            unset($this->context[$name]);
    }

    public function __get($name)
    {
        if (property_exists($this, $name))
            return $this->{$name};
        if (isset($this->context[$name]))
            return $this->context[$name];
        return null;
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->{$name} = $value;
        } else {
            $this->context[$name] = $value;
        }
    }

    /**
     * @param array|Collection $attributes
     * @return static
     */
    public static function make($attributes)
    {
        return new static($attributes);
    }

    /**
     * @param Throwable $e
     * @return static
     */
    public static function makeFromPrevious(\Throwable $e)
    {
        return new static($e->getMessage(), $e->getCode(), $e);
    }

    public function __construct($message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct('', $code, $previous);
        $this->exception = class_basename($this);
        if (filled($message)) {
            if (is_array($message)) {
                $this->withContext($message);
            } else {
                $this->message = $message;
            }
        }
        $this->localized_message = $this->getLocalizedMessage();
        if (empty($this->message)) {
            $this->refreshMessage();
        }
        if (empty($this->message)) {
            $this->message = $this->localized_message;
        }
    }

    /**
     * @return string
     */
    public function getLocalizedMessage()
    {
        return __("exception." . str_before(class_basename($this), "Exception"), $this->context);
    }

    /**
     * @return $this
     */
    public function refreshMessage()
    {
        // override in subclass
        // called during rendering
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->context +
            [
                'message' => $this->message,
                'code'    => $this->code,
            ];
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @param int $options
     * @return false|string
     * @throws \Exception
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);
        if (JSON_ERROR_NONE !== json_last_error())
            throw new \Exception('Error encoding exception [' . get_class($this) . ']  to JSON: ' . json_last_error_msg());
        return $json;
    }

    public function serialize()
    {
        return serialize(get_object_vars($this));
    }

    public function unserialize($serialized)
    {
        $this->withContext(unserialize($serialized));
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $context
     * @return $this
     */
    public function withContext($context)
    {
        foreach ($context as $key => $value)
            $this->{$key} = $value;
        return $this;
    }

    /**
     * @return $this
     */
    public function withoutContext()
    {
        $this->context = array_only($this->context, ['exception', 'localized_message']);
        return $this;
    }

    /**
     * @return array
     */
    public function context()
    {
        return $this->context;
    }

    /**
     * @return $this
     */
    public function withExceptionId()
    {
        $this->exception_id = telescope_exception();
        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response(tap($this)->refreshMessage()->withExceptionId()->toArray(), $this->getStatusCode())->withHeaders($this->headers);
    }
}
