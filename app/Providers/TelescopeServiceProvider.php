<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Laravel\Telescope\EntryType;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register()
    {
        $this->hideSensitiveRequestDetails();
        $this->tag();
        $this->filter();
    }

    private function tag()
    {
        Telescope::tag(function (IncomingEntry $entry) {
            switch ($entry->type) {
                case EntryType::REQUEST:
                    return $this->tagRequest($entry);
                case EntryType::LOG:
                    return $this->tagLog($entry);
                case EntryType::EXCEPTION:
                    return $this->tagException($entry);
            }
            return [];
        });
    }

    private function tagRequest(IncomingEntry $entry)
    {
        $tags = collect([
            "status:{$entry->content['response_status']}",
            "method:{$entry->content['method']}",
            "ip:{$entry->content['ip_address']}",
        ]);
        if ($route = Route::current()) {

            if ($route->named('api.*')) {
                $tags->push($route->getName(), ...explode('.', $route->getName()));
            }

            if ($phone = array_get($entry->content, 'payload.phone'))
                $tags->push("phone:$phone");
        }
        return $tags->toArray();
    }

    private function filter()
    {
        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->environment('local')) {
                return true;
            }

            $filter = function () use ($entry) {
                switch ($entry->type) {
                    case EntryType::REQUEST:
                        if (array_get($entry->content, 'status') >= 400 || array_get($entry->content, 'duration') > 2500)
                            return true;
                        return !starts_with(array_get($entry->content, 'uri'), ['/nova-api/', '/nova-vendor/']);
                    case EntryType::EXCEPTION:
                        if (starts_with($entry->content['class'], '\App\Exceptions\\'))
                            return true;
                }
                return false;
            };

            return $entry->isReportableException() ||
                $entry->isFailedRequest() ||
                $entry->isFailedJob() ||
                $entry->isScheduledTask() ||
                $entry->hasMonitoredTag() ||
                $filter();
        });
    }

    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }
        Telescope::hideRequestParameters(['_token', 'api_token']);
        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate()
    {
        Gate::define('viewTelescope', function (User $user) {
            return $user;
        });
    }

    private function tagException(IncomingEntry $entry)
    {
        $class = str_before(class_basename($entry->content['class']), 'Exception');
        return [$class];
    }

    private function tagLog(IncomingEntry $entry)
    {
        return [$entry->content['level']];
    }

}
