<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Laravel\Horizon\Horizon;
use Laravel\Horizon\HorizonApplicationServiceProvider;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    public function boot()
    {
        parent::boot();
        Horizon::routeSlackNotificationsTo(config('logging.channels.slack.url'));
    }

    protected function gate()
    {
        Gate::define('viewHorizon', function (User $user) {
            return $user;
        });
    }
}
