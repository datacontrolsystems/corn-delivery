<?php

namespace App\Logging;

class SlackLogger
{
    /**
     * Customize the given logger instance.
     *
     * @param \Illuminate\Log\Logger $logger
     * @return void
     */
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            $handler->pushProcessor(function ($record) {
//                $record['extra']['deployment'] = config('app.deployment');
                return $record;
            });
        }
    }

}
