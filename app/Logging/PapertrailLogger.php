<?php

namespace App\Logging;

class PapertrailLogger
{
    /**
     * @param \Illuminate\Log\Logger $logger
     * @return void
     */
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            $handler->pushProcessor(function ($record) {
                $record['extra']['project'] = config('app.name');
                $record['extra']['deployment'] = config('app.deployment');
                return $record;
            });
        }
    }

}
