<?php
/**
 * VonageVerifyChannel.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 16/6/21 6:22 μ.μ.
 */

namespace App\Channels;


use App\Exceptions\VerificationServiceException;
use App\Models\Client;
use App\Notifications\PhoneVerification;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use Psr\Http\Client\ClientExceptionInterface;
use Vonage\Client\Exception\Exception;
use Vonage\Client\Exception\Request;
use Vonage\Client\Exception\Server;

class VonageVerifyChannel
{

    /**
     * @param Client $user
     * @param \Illuminate\Notifications\Notification|PhoneVerification $notification
     * @return void
     */
    public function send($user, Notification $notification)
    {
        try {
            try {
                app('vonage')->cancel($user);
            } catch (\Throwable $e) {
            }
            $response = app('vonage')->send($user);
            $notification->setResponse($response);
        } catch (ClientExceptionInterface | Exception | Server | Request | VerificationServiceException $e) {
            Log::alert("Send verification SMS exception: " . $e->getMessage(), ['client' => $user->debug_name]);
            throw $e;
        }
    }
}
