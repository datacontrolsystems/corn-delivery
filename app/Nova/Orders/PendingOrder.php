<?php

namespace App\Nova\Orders;

use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Filters\SelectFilter;
use App\Nova\Resource;
use Illuminate\Http\Request;
use KirschbaumDevelopment\Nova\InlineSelect;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Monaye\NovaTippyField\Tippy;

class PendingOrder extends Resource
{
    public static $model = \App\Models\Order::class;
    public static $title = 'id';
    public static $subtitle = 'status_translated';
    public static $search = [
        'id', 'code', 'address', 'comments',
    ];
    public static $showPollingToggle = true;
    public static $pollingInterval = 30;
    public static $with = ['client'];
    public static $polling = true;

    /**
     * Apply any applicable orderings to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $orderings
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected static function applyOrderings($query, array $orderings)
    {
        // εμφανίζονται πρώτα οι παλαιότερες (πρώτες κατά προτεραιότητα) παραγγελίες
        // filter null orderings
        return parent::applyOrderings($query, array_filter($orderings) ?: ["id" => "asc"]);
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('status', 'pending');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Stack::make(__('Order'), [
                BelongsTo::make(__('Client'), 'client', \App\Nova\Orders\Client::class)->exceptOnForms()->withoutTrashed(),
                Currency::make(__('Charge'), 'charge')->exceptOnForms()->sortable(),
            ]),
            Stack::make(__('Products'),
                collect($this->resource->items_summary)->map(function ($item) {
                    return [
                        Line::make('', function () use ($item) {
                            return $item['quantity'] . ' x ' . $item['name'];
                        })->asHeading(),
                        empty($item['comments'])
                            ? null
                            : Line::make('', function () use ($item) {
                            return $item['comments'];
                        })->asBase(),
                    ];
                })->flatten()->filter()->toArray()
            ),
            InlineSelect::make(__('Status'), 'status')->options(\App\Models\Order::listStatuses())->displayUsingLabels()
                ->required()->rules('required')
                ->inlineOnIndex()
                ->inlineOnDetail(),
            Text::make(__('Address'), 'address')
                ->displayUsing(function ($address) {
                    return '<div style="white-space: nowrap;">' . wordwrap($address, 30, "<br>") . '</div>';
                })->asHtml(),
            Tippy::make(__('Comments'), 'comments')
                ->shouldShow()
                ->text(str_limit($this->comments, 30))
                ->tipContent($this->comments),
            HasMany::make(__('Order Items'), 'order_items', \App\Nova\Orders\OrderItem::class)->hideWhenCreating(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            CreatedAfter::make(),
            CreatedBefore::make(),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }

    public function creationFields(NovaRequest $request)
    {
        return [
            BelongsTo::make(__('Client'), 'client', \App\Nova\Orders\Client::class)->searchable()->withoutTrashed(),
            Select::make(__('Status'), 'status')->options(\App\Models\Order::listStatuses())->required()->rules('required'),
            Text::make(__('Address'), 'address'),
            Textarea::make(__('Comments'), 'comments'),
        ];
    }

    public static function label()
    {
        // ☎️ ♨ 🔥 🌽
        return '🔥️ ' . parent::label() . ' 🔥️';
    }
}
