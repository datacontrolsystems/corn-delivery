<?php

namespace App\Nova\Orders;

use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class OrderItem extends Resource
{
    public static $model = \App\Models\OrderItem::class;
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'name',
    ];
    public static $with = ['product', 'order'];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')->exceptOnForms(),
            Number::make(__('Quantity'), 'quantity')->min(0)->step(1)->required()->rules('required', 'integer'),
            Currency::make(__('Charge'), 'charge')->exceptOnForms(),
            BelongsTo::make(__('Product'), 'product', \App\Nova\Orders\Product::class)->required()->rules('required')->withoutTrashed(),
            BelongsTo::make(__('Order'), 'order', \App\Nova\Orders\Order::class)->exceptOnForms(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            CreatedAfter::make(),
            CreatedBefore::make(),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
