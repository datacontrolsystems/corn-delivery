<?php

namespace App\Nova\Orders;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Client extends Resource
{
    public static $model = \App\Models\Client::class;
    public static $title = 'name';
    public static $subtitle = 'phone';
    public static $search = [
        'id', 'name', 'phone',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')->required()->rules('required'),
            Text::make(__('Phone'), 'phone')->required()->rules('required'),
            DateTime::make(__('Phone Verified At'), 'phone_verified_at')->nullable(),
            Text::make(__('User'), function () {
                return "<a href='/admin/resources/users/{$this->id}' target='_blank' class='no-underline font-medium text-green'>" . __('User') . ' ' . $this->id . "</a>";
            })->asHtml(),
            HasMany::make(__('Orders'), 'orders', \App\Nova\Orders\Order::class),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
