<?php

namespace App\Nova\Orders;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Product extends Resource
{
    public static $model = \App\Models\Product::class;
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'name', 'description',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')->required()->rules('required'),
            Currency::make(__('Price'), 'price')->required()->rules('required', 'min:0')->sortable(),
            Textarea::make(__('Description'), 'description'),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
