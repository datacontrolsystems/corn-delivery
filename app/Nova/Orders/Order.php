<?php

namespace App\Nova\Orders;

use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Filters\SelectFilter;
use App\Nova\Resource;
use Illuminate\Http\Request;
use KirschbaumDevelopment\Nova\InlineSelect;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Text;
use Monaye\NovaTippyField\Tippy;

class Order extends Resource
{
    public static $model = \App\Models\Order::class;
    public static $title = 'id';
    public static $subtitle = 'status_translated';
    public static $search = [
        'id', 'code', 'address', 'comments',
    ];
    public static $showPollingToggle = true;
    public static $pollingInterval = 10;
    public static $with = ['client'];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make(__('Client'), 'client', \App\Nova\Orders\Client::class)->exceptOnForms()->withoutTrashed(),
            Currency::make(__('Charge'), 'charge')->exceptOnForms()->sortable(),
            Stack::make(__('Products'),
                collect($this->resource->items_summary)->map(function ($item) {
                    return [
                        Line::make('', function () use ($item) {
                            return $item['quantity'] . ' x ' . $item['name'];
                        })->asHeading(),
                        empty($item['comments'])
                            ? null
                            : Line::make('', function () use ($item) {
                            return $item['comments'];
                        })->asBase(),
                    ];
                })->flatten()->filter()->toArray()
            ),
            InlineSelect::make(__('Status'), 'status')->options(\App\Models\Order::listStatuses())->displayUsingLabels()
                ->required()->rules('required')
                ->inlineOnIndex()
                ->inlineOnDetail(), Text::make(__('Address'), 'address'),
            Tippy::make(__('Comments'), 'comments')
                ->text(str_limit($this->comments, 30))
                ->tipContent($this->comments),
            HasMany::make(__('Order Items'), 'order_items', \App\Nova\Orders\OrderItem::class)->hideWhenCreating(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            CreatedAfter::make(),
            CreatedBefore::make(),
            SelectFilter::make(__('Status'), 'status', array_flip(\App\Models\Order::listStatuses())),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
