<?php

namespace App\Nova\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class SelectFilter extends Filter
{
    public $component = 'select-filter';

    /**
     * @var string
     */
    public $column;
    /**
     * @var array
     */
    public $options;

    /**
     * @param string $name
     * @param string $column
     * @param array $options
     */
    public function __construct($name, $column, $options)
    {
        $this->name = $name;
        $this->column = $column;
        $this->options = $options;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $this->isMultiple()
            ? $query->when(array_keys(array_filter($value)), function (Builder $q, $selected) {
                return $q->whereIn($this->column, $selected);
            })
            : $query->where($this->column, $value);
    }

    public function options(Request $request)
    {
        return $this->options;
    }

    /**
     * Allow multiple choice.
     * @return $this
     */
    public function multiple()
    {
        $this->component = 'boolean-filter';
        return $this;
    }

    /**
     * @return bool
     */
    public function isMultiple()
    {
        return $this->component == 'boolean-filter';
    }

    public function key()
    {
        return class_basename($this) . '-' . $this->column;
    }

}
