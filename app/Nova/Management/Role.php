<?php

namespace App\Nova\Management;

use App\Nova\Actions\CopyPermissionsFromRole;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class Role extends Resource
{
    public static $model = \App\Models\Role::class;

    public static $title = 'name';

    public static $globallySearchable = false;

    public static $search = [
        'id', 'name',
    ];

    public function fields(Request $request)
    {
        $guardOptions = collect(config('auth.guards'))->mapWithKeys(function ($value, $key) {
            return [$key => $key];
        });
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')
                ->rules(['required', 'string', 'max:255'])
                ->creationRules('unique:' . config('permission.table_names.roles'))
                ->updateRules('unique:' . config('permission.table_names.roles') . ',name,{{resourceId}}'),
            Select::make(__('Guard Name'), 'guard_name')
                ->options($guardOptions->toArray())
                ->rules(['required', Rule::in($guardOptions)]),
            BelongsToMany::make(__('Permissions'), 'permissions', \App\Nova\Management\Permission::class)->searchable(),
            MorphToMany::make(__('Users'), 'users', \App\Nova\Management\User::class),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
            new CopyPermissionsFromRole,
        ];
    }
}
