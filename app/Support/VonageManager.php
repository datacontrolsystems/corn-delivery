<?php
/**
 * VonageManager.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 16/6/21 11:24 μ.μ.
 */

namespace App\Support;


use App\Exceptions\PhoneVerificationException;
use App\Exceptions\VerificationServiceException;
use App\Models\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Psr\Http\Client\ClientExceptionInterface;
use Vonage\Client\Exception\Exception;
use Vonage\Client\Exception\Request;
use Vonage\Client\Exception\Server;

class VonageManager
{

    /**
     * @param Client $client
     * @return \Vonage\Verify\Verification
     * @throws ClientExceptionInterface
     * @throws Exception
     * @throws Request
     * @throws Server
     * @throws VerificationServiceException
     */
    public function send(Client $client)
    {
        $request = new \Vonage\Verify\Request($client->phone, "Corn 🌽 Καλαμπόκι");
        // https://developer.nexmo.com/api/verify#verifyRequest
        $request->setCountry('GR');
        $request->setCodeLength(4); // 4 or 6
        $request->setLocale('el-gr');
        $request->setPinExpiry((int)config('services.nexmo.expiry'));
        $response = $this->getApiClient()->verify()->start($request);
        throw_unless($response->getStatus() == 0, VerificationServiceException::fromResponse($response));
        Log::info("Sent successful SMS verification: {$client->debug_name}");
        Redis::connection('default')->client()->set("client_{$client->id}_verification_request_id", $response->getRequestId());
        return $response;
    }

    /**
     * @param Client $client
     * @param string $code
     * @return \Vonage\Verify\Verification
     * @throws ClientExceptionInterface
     * @throws Exception
     * @throws Request
     * @throws Server
     * @throws PhoneVerificationException
     */
    public function verifyPhone(Client $client, $code)
    {
        $id = Redis::connection('default')->client()->get("client_{$client->id}_verification_request_id");
        throw_unless($id, PhoneVerificationException::class, ['client' => $client, 'message' => 'Verification RequestId not found or expired.']);
        try {
            $response = $this->getApiClient()->verify()->check($id, $code);
            throw_unless($response->getStatus() == 0, PhoneVerificationException::class, ['client' => $client, 'message' => 'SMS verification failed', 'request_id' => $response->getRequestId(), 'response_status' => $response->getStatus(), 'response_error_text' => array_get($response->getResponseData(), 'error_text')]);
            return $response;
        } catch (Request $e) {
            throw (new PhoneVerificationException($e->getMessage(), $e->getCode(), $e))->withContext(['client' => $client, 'request_id' => $id]);
        }
    }

    public function cancel(Client $client)
    {
        $id = Redis::connection('default')->client()->get("client_{$client->id}_verification_request_id");
        throw_unless($id, PhoneVerificationException::class, ['client' => $client, 'message' => 'Verification RequestId not found or expired.']);
        try {
            $response = $this->getApiClient()->verify()->cancel($id);
            throw_unless($response->getStatus() == 0, PhoneVerificationException::class, ['client' => $client, 'message' => 'Cancelling verification failed', 'request_id' => $response->getRequestId(), 'response_status' => $response->getStatus(), 'response_error_text' => array_get($response->getResponseData(), 'error_text')]);
            return $response;
        } catch (Request $e) {
            throw (new PhoneVerificationException($e->getMessage(), $e->getCode(), $e))->withContext(['client' => $client, 'request_id' => $id]);
        }
    }

    /**
     * @return \Vonage\Client
     */
    public function getApiClient()
    {
        return new \Vonage\Client(
            new \Vonage\Client\Credentials\Container(
                new \Vonage\Client\Credentials\Basic(
                    config('nexmo.api_key'),
                    config('nexmo.api_secret')
                )
            )
        );
    }
}
