<?php

namespace App\Http\Controllers\API;

use App\Exceptions\PhoneVerificationException;
use App\Exceptions\VerificationServiceException;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Notifications\PhoneVerification;
use Illuminate\Http\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Vonage\Client\Exception\Exception;
use Vonage\Client\Exception\Server;

class ClientController extends Controller
{
    public function current(Request $request)
    {
        return $request->user();
    }

    public function register(Request $request)
    {
        $attrs = $request->validate([
            'name'  => 'required',
            'phone' => 'required|digits_between:10,12',
        ]);
        $client = Client::updateOrCreate(array_only($attrs, ['phone']), array_only($attrs, ['name']));
        $client->saveOrFail();
        $client->notifyNow(new PhoneVerification());
        return response()
            ->json(['message' => 'Sending SMS verification'], $client->wasRecentlyCreated ? 201 : 200);
    }

    public function verifyPhone(Request $request, Client $client)
    {
        $data = $request->validate([
            'code' => 'required',
        ]);
        try {
            $client->verifyPhone($data['code']);
        } catch (PhoneVerificationException $e) {
            report($e);
            throw $e->withoutContext();
        } catch (ClientExceptionInterface | Server | Exception $e) {
            report($e);
            throw VerificationServiceException::makeFromPrevious($e);
        }
        $client->generateToken();
        $client->saveOrFail();
        return $client->makeVisible('api_token');
    }

}
