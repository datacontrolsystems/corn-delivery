<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Order::class, 'order');
    }

    public function index(Request $request)
    {
        return new OrderCollection($request->user()->orders()->latest()->paginate(100));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'comments'           => 'nullable',
            'address'            => 'required',
            'items'              => 'array|min:1',
            'items.*.quantity'   => 'required|integer|min:2',
            'items.*.product_id' => 'required|exists:App\Models\Product,id',
            'items.*.comments'   => 'nullable',
        ]);
        $order = new Order(array_only($data, ['comments', 'address']));
        $order->client()->associate($request->user());
        foreach ($data['items'] as $attrs) {
            $item = new OrderItem(array_only($attrs, ['quantity', 'comments', 'product_id']));
            $order->order_items->push($item);
        }
        $order->saveOrFail();
        return new OrderResource($order->refresh());
    }

    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    public function showLast(Request $request)
    {
        $last = $request->user()->orders()->latest()->first();
        abort_if(empty($last), 204);
        return new OrderResource($last);
    }

}
