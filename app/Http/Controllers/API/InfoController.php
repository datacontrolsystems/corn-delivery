<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    public function time()
    {
        $n = now();
        return $n->toArray() + [
                'weekOfMonth' => $n->weekOfMonth,
                'weekOfYear'  => $n->weekOfYear,
                'json'        => $n->toJSON(),
            ];
    }

}
