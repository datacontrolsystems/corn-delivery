<?php

namespace App\Http\Controllers\Nova;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class LoginController extends \Laravel\Nova\Http\Controllers\LoginController
{
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $report = Storage::disk('parking')->url(optional(session('user_session'))->getFilepathAttribute());

        $request->session()->invalidate();
        Session::put('generated-report', $report);

        return redirect($this->redirectPath());
    }

    public function username()
    {
        return 'username';
    }
}
