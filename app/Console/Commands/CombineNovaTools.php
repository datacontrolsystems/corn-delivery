<?php
/**
 * CombineNovaTools.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 20/2/20 4:46 μ.μ.
 */

namespace App\Console\Commands;

use App\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Laravel\Nova\Nova;

class CombineNovaTools extends Command
{
    protected $signature = 'nova:combine-tools {--r|remove : Delete existing combined files.} {--p|preview : Preview files as they are combined.}';
    protected $description = 'Combines all nova tools scripts and styles in nova-tools.[js|css] in public/[js/css]';

    const RESOURCES = ['allScripts' => 'js', 'allStyles' => 'css', 'themeStyles' => 'themes.css'];

    private $preview = false;

    public function handle()
    {
        $this->preview = $this->option('preview');
        $this->option('remove') ? $this->deleteFiles() : $this->combineTools();
    }

    private function combineTools()
    {
        // https://github.com/laravel/nova-issues/issues/1146#issuecomment-515891323
        $this->info('Combining nova tools js and css');
        Auth::login(Role::superAdmin()->users()->first());
        app()->handle(Request::create(Nova::path()));
        foreach (self::RESOURCES as $method => $type) {
            $content = '';
//            $content = "/*--- " . now()->toDateTimeString() . " ---*/\n";
            $hosted_online = 0;
            foreach (($files = Nova::{$method}()) as $name => $file) {
                if (starts_with($file, ['http://', 'https://'])) {
                    $hosted_online++;
                    $this->preview("$name -> $file Excluded due to online hosting.");
                    continue;
                }
                $this->preview("$name -> $file " . filesize_formatted($file));
                $content .= "/*-- $name --*/\n" . file_get_contents($file) . "\n";
            }
            $dest = public_path('vendor/nova/tools.' . $type);
            file_put_contents($dest, $content);
            $this->info("Combined " . count($files) . " files (excluded $hosted_online) " . filesize_formatted($dest) . " to $dest");
        }
    }

    protected function deleteFiles()
    {
        foreach (self::RESOURCES as $method => $type) {
            $dest = public_path('vendor/nova/tools.' . $type);
            if (file_exists($dest)) {
                unlink($dest);
                $this->info("Deleted $dest");
            } else {
                $this->warn("File does not exists $dest");
            }
        }
    }

    public function preview($string, $style = null, $verbosity = null)
    {
        if (!$this->preview)
            return;
        $this->line($string, $style, $verbosity);
    }
}
