<?php

namespace App\Observers;

use App\Models\Order;

class OrderObserver
{

    public function creating(Order $order)
    {
        $order->calculateCharge();
        $order->calculateItemsSummary();
    }

    public function created(Order $order)
    {
        $order->order_items()->saveMany($order->order_items);
    }

}
