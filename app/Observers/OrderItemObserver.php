<?php

namespace App\Observers;

use App\Models\OrderItem;

class OrderItemObserver
{
    public function saving(OrderItem $item)
    {
        if ($item->isDirty(['quantity', 'product_id'])) {
            $item->fillName();
            $item->calculateCharge();
        }
    }

    public function saved(OrderItem $item)
    {
        $item->order->calculateCharge()->calculateItemsSummary()->save();
    }
}
