<?php
/**
 * BasePolicy.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 7/12/20 2:12 μ.μ.
 */

namespace App\Policies;


abstract class BasePolicy
{
    use PolicyTrait;
}
