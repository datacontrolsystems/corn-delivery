<?php
/**
 * PolicyTrait.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 9/12/20 1:32 π.μ.
 */

namespace App\Policies;


use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

trait PolicyTrait
{
    use HandlesAuthorization;

    protected string $model;

    public function __construct()
    {
        $this->model = str_before(class_basename(static::class), 'Policy');
    }

    /**
     * @param User $user
     * @param      $ability
     * @return bool
     */
//    public function before($user, $ability)
//    {
//        if ($user->isCritical()) {
//            return true;
//        }
//    }

    protected function can(User $user, $ability, $model = null)
    {
        try {
            return $user->hasPermissionTo($model ? "$ability " . class_basename($model) : $ability);
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function viewAny(User $user)
    {
        try {
            return $user->hasPermissionTo("viewAny {$this->model}");
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function view(User $user, $model)
    {
        try {
            return $user->hasPermissionTo("view {$this->model}");
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function create(User $user)
    {
        try {
            return $user->hasPermissionTo("create {$this->model}");
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function update(User $user, $model)
    {
        try {
            return $user->hasPermissionTo("update {$this->model}");
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function delete(User $user, $model)
    {
        try {
            return $user->hasPermissionTo("delete {$this->model}");
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function restore(User $user, $model)
    {
        try {
            return $user->hasPermissionTo("restore {$this->model}");
        } catch (PermissionDoesNotExist $e) {
            return false;
        }
    }

    public function forceDelete(User $user, $model)
    {
        return false;
    }

}
