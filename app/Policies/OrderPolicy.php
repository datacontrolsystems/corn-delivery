<?php

namespace App\Policies;

use App\Models\Client;
use App\Models\Order;
use App\Models\User;

class OrderPolicy extends BasePolicy
{
    public function create(User $user)
    {
        return parent::create($user) || $user instanceof Client;
    }

    /**
     * @param User $user
     * @param Order $model
     * @return bool
     */
    public function update(User $user, $model)
    {
        return parent::update($user, $model) || $model->client_id == $user->id;
    }

    public function viewAny(User $user)
    {
        return parent::viewAny($user) || $user instanceof Client;
    }

    /**
     * @param User $user
     * @param Order $model
     * @return bool
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $model->client_id == $user->id;
    }

}
