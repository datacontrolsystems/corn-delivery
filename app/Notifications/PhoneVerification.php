<?php

namespace App\Notifications;

use App\Channels\VonageVerifyChannel;
use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class PhoneVerification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tries = 1;
    public $response = [];

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [VonageVerifyChannel::class, 'database'];
    }

    /**
     * @param Client $client
     * @return array
     */
    public function toDatabase($client)
    {
        return [
            'phone'    => $client->phone,
            'client'   => $client->debug_name,
            'response' => $this->response,
        ];
    }

    /**
     * @param \Vonage\Verify\Verification $response
     */
    public function setResponse($response)
    {
        $this->response = $response->toArray();
    }

    public function backoff()
    {
        return [2, 5, 10];
    }
}
