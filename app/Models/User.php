<?php

namespace App\Models;

use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use App\Scopes\UserTypeScope;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $user_type
 * @property string|null $username
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $phone_verified_at
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $api_token
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read User|null $creator
 * @property-read User|null $deleter
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-write mixed $plain_password
 * @property-read User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|User createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use ModelTraits, ModelCaching;
    use \Parental\HasChildren;

    protected $fillable = ['name', 'email', 'password', 'phone', 'user_type'];
    protected $hidden = ['password', 'remember_token', 'api_token'];
    protected $guarded = ['api_token'];
    protected $childColumn = 'user_type';
    protected $childTypes = [
        'user'   => User::class,
        'client' => Client::class,
    ];
    protected $dates = ['email_verified_at', 'phone_verified_at'];

    protected static function booted()
    {
        static::creating(function (User $user) {
            $user->{$user->getInheritanceColumn()} = $user->classToAlias(get_class($user));
        });
        static::addGlobalScope(new UserTypeScope());
    }

    /**
     * Get all users without any scopes. Includes systems and deleted.
     * @return \App\Models\User[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function everyone()
    {
        return static::cache(__FUNCTION__, function () {
            return (new static)->newQuery()->get();
        });
    }

    /**
     * @return User
     */
    public static function system()
    {
        return User::withoutGlobalScopes()->where('username', 'system')->firstOrFail();
    }

    /**
     * @return User
     */
    public static function cron()
    {
        return User::withoutGlobalScopes()->where('username', 'cron')->firstOrFail();
    }

    /**
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|User|null
     */
    public static function findByUsername($username)
    {
        return static::where('username', $username)->first();
    }

    /**
     * @param string $value
     * @return string
     */
    public function setPlainPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Generates a new api token if empty and saves the model.
     * @return $this
     */
    public function generateToken()
    {
        $this->attributes['api_token'] = str_random(60);
        $this->save();
        return $this;
    }

    public function can($ability, $arguments = [])
    {
        return Cache::tags(['can', config('permission.cache.key')])->remember("ability-$ability-{$this->id}", config('cache.duration'), function () use ($ability, $arguments) {
            return app(Gate::class)->forUser($this)->check($ability, $arguments);
        });
    }

}
