<?php

namespace App\Models;

use App\Exceptions\InvalidDataException;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property string $status
 * @property float $charge
 * @property string|null $comments
 * @property string $address
 * @property int $client_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read string $status_translated
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $order_items
 * @property-read int|null $order_items_count
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Order createdBy($userId)
 * @method static \Database\Factories\OrderFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    use ModelTraits;
    use HasFactory;

    public const STATUSES = ['pending', 'completed', 'cancelled'];

    protected $fillable = ['comments', 'address'];
    protected $attributes = [
        'status'        => 'pending',
        'items_summary' => '{}',
    ];
    protected $casts = [
        'items_summary' => 'array',
    ];
    protected $hidden = ['items_summary'];

    /**
     * @return array
     */
    public static function listStatuses()
    {
        return array_combine(self::STATUSES, array_map('__', self::STATUSES));
    }

    /**
     * @return $this
     */
    public function calculateCharge()
    {
        $this->attributes['charge'] = $this->order_items->sum('charge');
        return $this;
    }

    /**
     * @return $this
     */
    public function calculateItemsSummary()
    {
        $this->items_summary = $this->order_items->sortBy('product_id')->toArray();
        return $this;
    }

    /**
     * @param string $value
     * @return string
     */
    public function setStatusAttribute($value)
    {
        throw_unless(in_array($value, self::STATUSES), InvalidDataException::class, "Invalid order status '$value'");
        return $this->attributes['status'] = $value;
    }

    /**
     * @return string
     */
    public function getStatusTranslatedAttribute()
    {
        return __($this->status);
    }

    public function order_items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
