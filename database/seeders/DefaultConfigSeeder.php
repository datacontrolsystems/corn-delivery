<?php

namespace Database\Seeders;

use App\Models\ConfigModel;
use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultConfigSeeder extends Seeder
{
    public $configs = [
    ];

    public function run()
    {
        while_logged_in(function () {
            $this->loadConfigs();
        }, User::system());
    }

    protected function loadConfigs()
    {
        foreach ($this->configs as $config) {
            ConfigModel::createAndLoadKey($config);
        }
    }

}
