<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['name' => 'Ψητό Καλαμπόκι', 'price' => 2],
            ['name' => 'Βραστό Καλαμπόκι', 'price' => 2],
        ])->each(function ($attrs) {
            Product::create($attrs);
        });
    }
}
