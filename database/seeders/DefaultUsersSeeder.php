<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $superAdmin = Role::superAdmin();
        User::system()->roles()->save($superAdmin);
        User::cron()->roles()->save($superAdmin);
        collect([
            ['name' => 'Theofanis', 'username' => 'theofanis', 'email' => 'vardtheo@virtual-net.gr', 'plain_password' => 'DataControlS', 'roles' => Role::superAdmin()],
            ['name' => 'Γ. Μαραγκάκης', 'username' => 'maragkakis', 'email' => 'gmaragkakis@dcs.com.gr', 'plain_password' => 'DataControlS', 'roles' => Role::superAdmin()],
            ['name' => 'Δ. Κοζάκης', 'username' => 'kozakis', 'email' => 'dkozakis@virtual-net.gr', 'plain_password' => 'DataControlS', 'roles' => Role::superAdmin()],
            ['name' => 'Γ. Σιλβεστρίδης', 'username' => 'ioannissilvestridis', 'email' => 'ioannissilvestridis@gmail.com', 'plain_password' => 'Corn1.', 'roles' => Role::superAdmin()],
        ])->each(function ($attrs) {
            $roles = Arr::pull($attrs, 'roles');
            $u = User::create($attrs);
            $u->assignRole($roles);
        });
    }

}
