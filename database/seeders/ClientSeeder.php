<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    public function run()
    {
        if (app()->environment('local')) {
            collect([
                ['name' => 'Γιώργος Δοκιμή', 'phone' => '6912345678', 'api_token' => 'nFOtmaoTJraYfrC4Ztz2p3yAayAVfydQDdOpVxnip3Mey4xgFBnCPantRyfI', 'phone_verified_at' => now()],
                ['name' => 'Κατερίνα Δοκιμή', 'phone' => '6911111111', 'api_token' => '6Qq152zrFIfRUzCbQseQblaQYOHVKdqFrhHyDDknieao3hc5Fgk6ypTJs2ik', 'phone_verified_at' => now()],
                ['name' => 'Μάκης Δοκιμή', 'phone' => '6999999999', 'api_token' => 'yWsu2NjfI5gqpkCvi5E1tE7fZt3TdY54PDz2n9wnFVS275jB78MeohY29tpq'],
            ])->each([Client::class, 'create']);
        }
    }
}
