<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRolesWithPermissionsSeeder extends Seeder
{
    /**
     * @var Role
     */
    protected $superAdmin;
    /**
     * @var Role
     */
    protected $admin;

    public function run()
    {
        $this->setSuperAdminPermissions();
        $this->setAdminPermissions();
    }

    protected function setSuperAdminPermissions()
    {
        $this->superAdmin = Role::superAdmin()->syncPermissions(Permission::all());
    }

    protected function setAdminPermissions()
    {
        $revokePermissions = flatten_permissions_map([
            'super-admin',
            'run commands',
        ]);
        $permissions = $this->superAdmin->getAllPermissions()->reject(function (Permission $permission) use ($revokePermissions) {
            return in_array($permission->name, $revokePermissions);
        });
        $this->admin = Role::admin()->syncPermissions($permissions);
    }

}
