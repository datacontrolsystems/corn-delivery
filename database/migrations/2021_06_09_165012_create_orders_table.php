<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id()->startingValue(1000);
            $table->string('status');
            $table->double('charge', 8, 2);
            $table->string('comments')->nullable();
            $table->string('address');
            $table->foreignId('client_id')->constrained('users');
            $table->json('items_summary');
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
