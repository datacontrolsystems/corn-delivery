<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('quantity');
            $table->double('charge', 8, 2);
            $table->string('comments')->nullable();
            $table->foreignId('product_id')->constrained();
            $table->foreignId('order_id')->constrained();
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
