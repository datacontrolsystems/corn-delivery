<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigTable extends Migration
{

    public function up()
    {
        Schema::create('config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->json('json_value');
            $table->text('serialized_value');
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('config');
    }

}
