<?php
/**
 * report-part.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 19/11/19 10:41 π.μ.
 */

return [
    'actions-per-customer-type'        => 'Κινήσεις ανά Τύπο Πελάτη',
    'barrier-openings'                 => 'Ανοίγματα Μπάρας',
    'invoices'                         => 'Παραστατικά Αναλυτικά',
    'invoices-per-cash-desk'           => 'Παραστατικά ανά Ταμείο',
    'simple-invoices'                  => 'Παραστατικά Απλοποιημένα',
    'ticket-reprints'                  => 'Επανεκτυπώσεις Εισιτηρίων',
    'occupancy-stats-per-parking-zone' => 'Στατιστικά Πληρότητας ανά Ζώνη',
    'duration-stats'                   => 'Στατιστικά Διάρκειας Παραμονής',
    'invoices-for-ticket-charges'      => 'Παραστατικά Εισιτηρίων Αναλυτικά',
    'invoices-for-subscriptions'       => 'Παραστατικά Συνδρομές Αναλυτικά',
    'customers-vehicles'               => 'Πελάτες με Οχήματα',
    'customers-data'                   => 'Πελάτες Αναλυτικά',
    'customer-balances'                => 'Υπόλοιπα Πελατών',
    'new-customers'                    => 'Νέοι Πελάτες',
    'exits-per-terminal'               => 'Έξοδοι ανά Τερματικό',
];
