<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Καλαμπόκια</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: Nunito,ui-sans-serif,system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .bottom-right {
            position: absolute;
            right: 10px;
            bottom: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="top-right links">
            @auth
                <a href="{{ route('nova.logout') }}">{{ __('Logout') }}</a>
            @else
                <a href="{{ route('nova.login') }}">{{ __('Login') }}</a>
            @endauth
        </div>

        <div class="content">
            <div class="title m-b-md">
                Καλαμπόκια
                <br>
                🌽
            </div>

            <div class="links">
                <a href="/admin/resources/pending-orders">Ανοιχτές Παραγγελίες</a>
                <a href="/admin">Διαχείριση</a>
            </div>

            <div class="links bottom-right">
                <a href="https://documenter.getpostman.com/view/1408338/TzeUoUrJ" target="_blank">API</a>
            </div>
        </div>
    </div>
</body>
</html>
