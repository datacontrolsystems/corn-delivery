<dropdown-trigger class="h-9 flex items-center">
    <img
        src="https://ui-avatars.com/api/?name={{ urlencode($user->name) }}&color=7F9CF5&background=EBF4FF"
        class="rounded-full w-8 h-8 mr-3"
    />

    <span class="text-90">
        {{ $user->name ?? $user->email ?? __('Nova User') }}
    </span>
</dropdown-trigger>

<dropdown-menu slot="menu" width="200" direction="rtl">
    <ul class="list-reset">
        <li>
            <router-link :to="{ name: 'detail', params: { resourceName: 'users', resourceId: '{{ $user->id }}' } }" class="block no-underline text-90 hover:bg-30 p-3">
                {{ __('Account') }}
            </router-link>
        </li>
        <li>
            <a href="{{ route('nova.logout') }}" class="block no-underline text-90 hover:bg-30 p-3">
                {{ __('Logout') }}
            </a>
        </li>
    </ul>
</dropdown-menu>
