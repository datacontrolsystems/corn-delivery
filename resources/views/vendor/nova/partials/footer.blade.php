<p class="mt-8 text-center text-xs text-80">
    <a href="http://virtual-net.gr" class="text-primary dim no-underline">Virtual-Net</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Corn Delivery
</p>
